const fs = require('fs');
const path = require('path');
const utils = require('../utils');
const {uploadPath} = require('../config');
const {videoService} = require('../services/videoService');

module.exports = {
    list(app) {
        app.get('/api/video', async (req, res) => {
            try {
                const filter = {};
                if(req.query.published) {
                    filter.published = req.query.published === 'true';
                }
                if(req.query.search) {
                    const search = req.query.search.trim();
                    search && (filter.search = search);
                }
                let result = await videoService.list({filter});
                res.json(result);
            } catch (e) {
                res.status(500).json(e);
            }
        });
    },

    create(app) {
        app.post('/api/video/create', async (req, res) => {
            if (req.session.user.role !== 'admin') {
                return res.status(403).json({error: 'Must be admin'});
            }
            let filePath;
            try {
                const title = req.query.title;
                if (!title) {
                    throw {error: 'Video title is required'};
                }
                if (!req.query.filename) {
                    throw {error: 'Video file name is required'};
                }
                const match = req.query.filename.match(/(\.[^.]+$)/);
                if (!match) {
                    throw {error: 'Video must contain extension'};
                }
                const fileName = `${utils.rndHash()}${match[1]}`;
                filePath = path.resolve(`${uploadPath}/${fileName}`);
                const writeStream = fs.createWriteStream(filePath);
                await new Promise((resolve, reject) => {
                    writeStream.on('open', resolve);
                    writeStream.on('error', reject);
                });
                req.pipe(writeStream);
                await new Promise((resolve, reject) => {
                    writeStream.on('finish', resolve);
                    writeStream.on('error', reject);
                });
                let result = await videoService.create({fileName, title});
                res.json(result);
            } catch (e) {
                fs.unlink(filePath, () => {
                    res.status(500).json(e);
                });
            }
        });
    },

    get(app) {
        app.get('/api/video/:id', async (req, res) => {
            try {
                const id = req.params.id;
                const result = await videoService.get(id);
                res.json(result);
            } catch (e) {
                res.status(500).json(e);
            }
        });
    },

    publish(app) {
        app.post('/api/video/publish', async (req, res) => {
            if (req.session.user.role !== 'admin') {
                return res.status(403).json({error: 'Must be admin'});
            }
            try {
                await videoService.publish(req.body);
                res.json({});
            } catch (e) {
                res.status(500).json(e);
            }
        });
    },

    edit(app) {
        app.post('/api/video/edit', async (req, res) => {
            if (req.session.user.role !== 'admin') {
                return res.status(403).json({error: 'Must be admin'});
            }
            try {
                await videoService.edit(req.body);
                res.json({});
            } catch (e) {
                res.status(500).json(e);
            }
        });
    },

    delete(app) {
        app.delete('/api/video/:id', async (req, res) => {
            if (req.session.user.role !== 'admin') {
                return res.status(403).json({error: 'Must be admin'});
            }
            try {
                await videoService.delete(req.params.id);
                res.json({});
            } catch (e) {
                res.status(500).json(e);
            }
        });
    }
};