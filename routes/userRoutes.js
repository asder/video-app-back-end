const {userService} = require('../services/userService');

module.exports = {
    logout(app) {
        app.get('/api/user/logout', (req, res) => {
            req.session.destroy((err) => {
                const status = err ? 500 : 200;
                res.status(status).json(err || {});
            });
        });
    },

    register(app) {
        app.post('/api/user/register', async (req, res) => {
            try {
                const {login, password, name} = req.body;
                const {id, role} = await userService.register({login, password, name});
                req.session.user = {
                    id,
                    role
                };
                res.json({role});
            } catch (e) {
                res.status(500).json(e);
            }
        });
    },

    login(app) {
        app.post('/api/user/login', async (req, res) => {
            try {
                const {login, password} = req.body;
                const {id, role} = await userService.login({login, password});
                req.session.user = {
                    id,
                    role
                };
                res.json({role});
            } catch (e) {
                res.status(500).json(e);
            }
        });
    }
};