const userRoutes = require('./userRoutes');
const videoRoutes = require('./videoRoutes');
const manualRoutes = require('./manualRoutes');

module.exports = (app) => {
    /**
     * CSRF middleware for all routes. Front-end need to pass X-CSRF header
     */
    app.use((req, res, next) => {
        if(!req.get('X-CSRF')) {
            return res.status(400).json({error: 'X-CSRF header required'});
        }
        next();
    });

    userRoutes.register(app);
    userRoutes.login(app);
    userRoutes.logout(app);

    videoRoutes.list(app);
    videoRoutes.get(app);

    manualRoutes.list(app);
    manualRoutes.get(app);
    manualRoutes.groupNames(app);

    /**
     * Auth middleware, next routes will require authorisation
     */
    app.use((req, res, next) => {
        if(!req.session.user) {
            return res.status(403).json({error: 'Not_authorized'});
        }
        next();
    });

    videoRoutes.create(app);
    videoRoutes.edit(app);
    videoRoutes.delete(app);
    videoRoutes.publish(app);

    manualRoutes.create(app);
    manualRoutes.edit(app);
    manualRoutes.delete(app);
};