const fs = require('fs');
const path = require('path');
const utils = require('../utils');
const {uploadPath} = require('../config');
const {manualService} = require('../services/manualService');

module.exports = {
    groupNames(app) {
        app.get('/api/manual/groupNames', async (req, res) => {
            try {
                let result = await manualService.groupNames();
                res.json(result);
            } catch (e) {
                res.status(500).json(e);
            }
        });
    },

    list(app) {
        app.get('/api/manual', async (req, res) => {
            try {
                let result = await manualService.list();
                res.json(result);
            } catch (e) {
                res.status(500).json(e);
            }
        });
    },

    create(app) {
        app.post('/api/manual/create', async (req, res) => {
            if (req.session.user.role !== 'admin') {
                return res.status(403).json({error: 'Must be admin'});
            }
            let filePath;
            try {
                const title = req.query.title;
                if (!title) {
                    throw {error: 'Manual title is required'};
                }
                if (!req.query.filename) {
                    throw {error: 'Manual file name is required'};
                }
                const match = req.query.filename.match(/(\.[^.]+$)/);
                if (!match) {
                    throw {error: 'Manual must contain extension'};
                }
                const groupName = req.query.groupName;
                if(!groupName) {
                    throw {error: 'Manual groupName is required'};
                }
                const fileName = `${utils.rndHash()}${match[1]}`;
                filePath = path.resolve(`${uploadPath}/${fileName}`);
                const writeStream = fs.createWriteStream(filePath);
                await new Promise((resolve, reject) => {
                    writeStream.on('open', resolve);
                    writeStream.on('error', reject);
                });
                req.pipe(writeStream);
                await new Promise((resolve, reject) => {
                    writeStream.on('finish', resolve);
                    writeStream.on('error', reject);
                });
                let result = await manualService.create({fileName, title, groupName});
                res.json(result);
            } catch (e) {
                fs.unlink(filePath, () => {
                    res.status(500).json(e);
                });
            }
        });
    },

    get(app) {
        app.get('/api/manual/:id', async (req, res) => {
            try {
                const id = req.params.id;
                const result = await manualService.get(id);
                res.json(result);
            } catch (e) {
                res.status(500).json(e);
            }
        });
    },

    edit(app) {
        app.post('/api/manual/edit', async (req, res) => {
            if (req.session.user.role !== 'admin') {
                return res.status(403).json({error: 'Must be admin'});
            }
            try {
                const {id, groupName, title} = req.body;
                await manualService.edit({id, groupName, title});
                res.json({});
            } catch (e) {
                res.status(500).json(e);
            }
        });
    },

    delete(app) {
        app.delete('/api/manual/:id', async (req, res) => {
            if (req.session.user.role !== 'admin') {
                return res.status(403).json({error: 'Must be admin'});
            }
            try {
                await manualService.delete(req.params.id);
                res.json({});
            } catch (e) {
                res.status(500).json(e);
            }
        });
    }
};