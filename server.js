const express = require('express');
const app = express();
const helmet = require('helmet');
const {port, cookieSecret, uploadPath} = require('./config');
const routes = require('./routes');
const session = require('express-session');
const bodyParser = require('body-parser');
const path = require('path');

app.use(helmet());
app.set('trust proxy', 1);
app.use(session({
    secret: cookieSecret,
    resave: false,
    saveUninitialized: true,
    cookie: {  maxAge: 24 * 60 * 60 * 30 * 1000},
}));
app.use(bodyParser.json({limit: '5mb'}));

/**
 * serve upload files for development
 */
if(process.env.NODE_ENV !== 'production') {
    app.use('/upload/', express.static(path.resolve(`${uploadPath}`)));
}

routes(app);

app.listen(port, () => {
    console.log(`Video app is listening at http://localhost:${port}`)
});

process.on('uncaughtException', (err) => {
    console.error(err);
});