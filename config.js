const devMariaDbConfig = {
    host: 'contabo.netvisionllc.com',
    user: 'root',
    password: '2423a818b8',
    database: 'videoapp',
    connectionLimit: 5
};

const prodMariaDbConfig = {
    host: 'localhost',
    user: 'root',
    password: '2423a818b8',
    database: 'videoapp',
    connectionLimit: 5
};

const mariaDbConfig = process.env.NODE_ENV === 'production' ? prodMariaDbConfig : devMariaDbConfig;

module.exports = {
    port: process.env.PORT || 4000,
    cookieSecret: '93d7b2351b95dd9424cc248bc27f69c6',
    hashSalt: '6ec96844096539cbae841bfd03bc7321',
    mariaDbConfig,
    uploadPath: '../upload'
};