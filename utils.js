const {hashSalt} = require('./config');
const crypto = require('crypto');

module.exports = {
    toQueryString: function(obj) {
        return Object.keys(obj)
            .reduce((result, key) => {
                if(obj[key] !== undefined) {
                    result.push(`${encodeURIComponent(key)}=${encodeURIComponent(obj[key])}`);
                }
                return result;
            }, [])
            .join('&');
    },
    rndHash: function() {
        return crypto.randomBytes(32).toString('hex');
    },
    hashPassword: function(password) {
        return crypto.createHash('sha256').update(`${password}${hashSalt}`).digest('hex');
    },
};