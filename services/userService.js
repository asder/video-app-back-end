const BaseService = require('./baseService');
const utils = require('../utils');

class UserService extends BaseService {

    async register({login, password, name}) {
        try {
            if(!password || password.length < 8) {
                throw {error: 'Minimum password length is 8 symbols'};
            }
            if(!login || login.length < 2) {
                throw {error: 'Minimum login length is 2 symbols'};
            }
            if(!name) {
                throw {error: 'Name is required'};
            }
            let res = await this.query(`SELECT id FROM va_users WHERE login=?`, [login]);
            if(res && res.length) {
                throw {error: 'Login already exists'};
            }
            const hashedPassword = utils.hashPassword(password);
            res = await this.query(`INSERT INTO va_users (login, password, name, role) VALUES(?, ?, ?, 'user') RETURNING id, role`, [login, hashedPassword, name]);
            return {id: res[0].id, role: res[0].role};
        } catch (e) {
            throw e;
        }
    }

    async login(params) {
        try {
            if(!params.password) {
                throw {error: 'Password is required'};
            }
            if(!params.login) {
                throw {error: 'Login is required'};
            }
            let res = await this.query(`SELECT id, role, password FROM va_users WHERE login=?`, [params.login]);
            if(!res || !res.length) {
                throw {error: 'No such user'};
            }
            let {id, role, password} = res[0];
            const hashedPassword = utils.hashPassword(params.password);
            if(hashedPassword !== password) {
                throw {error: 'Wrong password'};
            }
            return {id, role};
        } catch (e) {
            throw e;
        }
    }
}

const userService = new UserService();

module.exports = {userService};