const BaseService = require('./baseService');
const fs = require('fs').promises;
const path = require('path');
const {uploadPath} = require('../config');

class VideService extends BaseService {

    async create({fileName, title}) {
        try {
            if(!title) {
                throw {error: 'Video title is required'};
            }
            if(!fileName) {
                throw {error: 'Video file name is required'};
            }
            let res = await this.query(`INSERT INTO va_video (title, fileName) VALUES(?, ?) RETURNING id`, [title, fileName]);
            return {id: res[0].id, title, fileName};
        } catch (e) {
            throw e;
        }
    }

    async publish({published, id}) {
        try {
            if(!id) {
                throw {error: 'Video id is required'};
            }
            if(typeof published === "undefined") {
                throw {error: 'published param is required'};
            }
            await this.query(`UPDATE va_video SET published=? WHERE id=?`, [published, id]);
        } catch (e) {
            throw e;
        }
    }

    async edit({id, title, module, _function, description, thumbnail, duration, captions}) {
        try {
            if(!id) {
                throw {error: 'Video id is required'};
            }
            if(!title) {
                throw {error: 'Video title is required'};
            }
            if(!module) {
                throw {error: 'Video module is required'};
            }
            if(!_function) {
                throw {error: 'Video function is required'};
            }
            if(!description) {
                throw {error: 'Video description is required'};
            }
            if(!duration) {
                throw {error: 'Video duration is required'};
            }
            if(!captions) {
                throw {error: 'Video captions are required'};
            }
            captions = JSON.stringify(captions);
            let res = await this.query(`SELECT id, title, fileName, description, thumbnail, duration, module, function FROM va_video WHERE id=?`, [id]);
            if(!res.length) {
                throw {error: 'Video id not found'};
            }
            res = res[0];
            let thumbnailSet = '';
            if(thumbnail) {
                let fileName = res.fileName.replace(/\.[^.]+$/, '.png');
                await fs.writeFile(path.resolve(`${uploadPath}/${fileName}`), thumbnail.replace(/^data:image\/png;base64,/, ''), 'base64');
                thumbnailSet = `, thumbnail='${fileName}'`
            }
            await this.query(`UPDATE va_video SET title=?, module=?, function=?, description=?, duration=?, captions=? ${thumbnailSet} WHERE id=?`,
                [title, module, _function, description, duration, captions, id]);

        } catch (e) {
            throw e;
        }
    }

    async list({filter}) {
        let whereState = ` WHERE 1=1 `;
        if(filter.hasOwnProperty('published')) {
            whereState += ` AND published=${filter.published?'true':'false'} `;
        }
        const searchConditions = [];
        let searchWords;
        if(filter.search) {
            searchWords = filter.search.split(/\s+/);
            if(searchWords.length > 1) {
                searchConditions.push(` AND MATCH(title) AGAINST(? IN BOOLEAN MODE) `);
                searchConditions.push(` AND MATCH(description) AGAINST(? IN BOOLEAN MODE) `);
            } else {
                searchWords[0] = `%${searchWords[0]}%`;
                searchConditions.push(` AND title LIKE ? `);
                searchConditions.push(` AND module LIKE ? `);
                searchConditions.push(` AND description LIKE ? `);
            }
            searchWords = searchWords.join(',');
        }
        try {
            let result;
            let where;
            do {
                where = whereState;
                searchConditions.length && (where += searchConditions.shift());
                result = await this.query(
                    `SELECT id, title, thumbnail, duration, module, function, published FROM va_video ${where} ORDER BY id DESC`,
                    [searchWords]
                );
            } while (searchConditions.length && !result.length);
            return result;
        } catch (e) {
            throw e;
        }
    }

    async get(id) {
        try {
            let result = await this.query(`SELECT id, title, fileName, description, thumbnail, duration, module, function, captions FROM va_video WHERE id=?`, [id]);
            if(!result.length) {
                throw {error: 'No data'};
            }
            return result[0];
        } catch (e) {
            throw e;
        }
    }

    async delete(id) {
        try {
            if(!id) {
                throw {error: 'id is required'};
            }
            let result = await this.get(id);
            try {
                await fs.unlink(path.resolve(`${uploadPath}/${result.thumbnail}`));
            } catch (e) {
                console.error(`Couldn't remove ${result.thumbnail}`);
            }
            try {
                await fs.unlink(path.resolve(`${uploadPath}/${result.fileName}`));
            } catch (e) {
                console.error(`Couldn't remove ${result.fileName}`);
            }
            await this.query('DELETE FROM va_video WHERE id=?', [id]);
        } catch (e) {
            throw e;
        }
    }
}

const videoService = new VideService();

module.exports = {videoService};