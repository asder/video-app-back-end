const mariadb = require('mariadb');
const {mariaDbConfig} = require('../config')
const pool = mariadb.createPool(mariaDbConfig);

class BaseService {
    async query(sql, params) {
        let conn;
        let res;
        try {
            conn = await pool.getConnection();
            res = await conn.query(sql, params);
        } catch (err) {
            throw err;
        } finally {
            conn && conn.end();
        }
        return res;
    }
}

module.exports = BaseService;