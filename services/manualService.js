const BaseService = require('./baseService');
const fs = require('fs').promises;
const path = require('path');
const {uploadPath} = require('../config');

class ManualService extends BaseService {

    async groupNames() {
        try {
            let result = await this.query(`SELECT DISTINCT groupName FROM va_manual ORDER BY groupName`);
            return result;
        } catch (e) {
            throw e;
        }
    }

    async create({fileName, title, groupName}) {
        try {
            if(!title) {
                throw {error: 'Manual title is required'};
            }
            if(!groupName) {
                throw {error: 'Manual groupName is required'};
            }
            if(!fileName) {
                throw {error: 'Manual file name is required'};
            }
            let res = await this.query(`INSERT INTO va_manual (title, fileName, groupName) VALUES(?, ?, ?) RETURNING id`, [title, fileName, groupName]);
            return {id: res[0].id, title, fileName, groupName};
        } catch (e) {
            throw e;
        }
    }

    async edit({id, groupName, title}) {
        try {
            if(!title) {
                throw {error: 'Manual title is required'};
            }
            if(!id) {
                throw {error: 'Manual id is required'};
            }
            if(!groupName) {
                throw {error: 'Manual groupName is required'};
            }
            await this.query(`UPDATE va_manual SET title=?, groupName=? WHERE id=?`,
                [title, groupName, id]);

        } catch (e) {
            throw e;
        }
    }

    async list() {
        try {
            let result = await this.query(`SELECT id, groupName, title, fileName FROM va_manual ORDER BY id DESC`);
            return result;
        } catch (e) {
            throw e;
        }
    }

    async get(id) {
        try {
            let result = await this.query(`SELECT id, groupName, title, fileName FROM va_manual WHERE id=?`, [id]);
            if(!result.length) {
                throw {error: 'No data'};
            }
            return result[0];
        } catch (e) {
            throw e;
        }
    }

    async delete(id) {
        try {
            if(!id) {
                throw {error: 'id is required'};
            }
            let result = await this.get(id);
            try {
                await fs.unlink(path.resolve(`${uploadPath}/${result.fileName}`));
            } catch (e) {
                console.error(`Couldn't remove ${result.fileName}`);
            }
            await this.query('DELETE FROM va_manual WHERE id=?', [id]);
        } catch (e) {
            throw e;
        }
    }
}

const manualService = new ManualService();

module.exports = {manualService};