CREATE DATABASE videoapp;
CREATE TABLE va_users (
    id int(11) PRIMARY KEY AUTO_INCREMENT,
    login varchar(128) NOT NULL UNIQUE,
    password varchar(128) NOT NULL,
    name varchar(128) NOT NULL,
    role ENUM('admin', 'user')
);
CREATE TABLE va_video (
    id int(11) PRIMARY KEY AUTO_INCREMENT,
    title varchar(128) NOT NULL,
    fileName varchar(128) NOT NULL,
    description mediumtext DEFAULT NULL,
    thumbnail varchar(128) DEFAULT NULL,
    duration int(11) DEFAULT NULL,
    module varchar(64) DEFAULT NULL,
    function varchar(64) DEFAULT NULL,
    captions JSON NOT NULL DEFAULT '{}',
    published boolean DEFAULT false
);

ALTER TABLE va_video ADD FULLTEXT INDEX idx_title (title);
ALTER TABLE va_video ADD FULLTEXT INDEX idx_description (description);

CREATE TABLE va_manual (
    id int(11) PRIMARY KEY AUTO_INCREMENT,
    groupName varchar(64) DEFAULT NULL,
    title varchar(128) NOT NULL,
    fileName varchar(128) NOT NULL
);

ALTER TABLE va_manual ADD INDEX idx_groupName (groupName);